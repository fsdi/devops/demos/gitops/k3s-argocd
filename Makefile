#!/usr/bin/env make

CLUSTER_NAME				= local-dev
APP_NS						= $(CLUSTER_NAME)
PROMETHEUS_CHART_VERSION 	= "14.11.0"
GRAFANA_CHART_VERSION		= "6.17.3"
ARGOCD_CHART_VERTION		= "3.26.3"
DEV_CLUSTER_NAME 			= "dev-ng"



.PHONY: start-monitor helm-grafana helm-prometheus 

start-local-cluster:
	k3d cluster create $(CLUSTER_NAME) --api-port 6550 --servers 1 --agents 2 --port 80:80@loadbalancer --port 443:80@loadbalancer --wait || k3d cluster list | grep $(CLUSTER_NAME) && k3d cluster start $(CLUSTER_NAME)
	k3d kubeconfig merge $(CLUSTER_NAME) --kubeconfig-merge-default --kubeconfig-switch-context
	kubectx k3d-$(CLUSTER_NAME)

_create_ns:
	@kubectl get ns | grep $(NS) || kubectl create ns $(NS)

helm-argo-cd: 
	@kubectl get ns | grep argocd || kubectl create ns argocd
	@helm repo list | grep argo || \
	 helm repo add argo https://argoproj.github.io/argo-helm
	@helm search repo argo --version=$(ARGOCD_CHART_VERTION) || helm repo update
	@helm upgrade argocd --install \
		--namespace argocd \
		--version $(ARGOCD_CHART_VERTION) \
	  	argo/argo-cd \
		-f assets/charts/argo-cd/values.yaml \
		-f assets/charts/argo-cd/override.yaml \
		-f assets/charts/argo-cd/dev-mode.yaml

helm-prometheus: 
	@helm repo list | grep prometheus-community || \
	 helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
	@helm search repo prometheus-community --version=$(PROMETHEUS_CHART_VERSION) || helm repo update
	@helm upgrade prometheus --install \
		--namespace prometheus \
		--create-namespace \
		--version $(PROMETHEUS_CHART_VERSION) \
	  	prometheus-community/prometheus \
		-f assets/charts/prometheus/values.yaml \
		-f assets/charts/prometheus/override.yaml

argo-all: start-local-cluster helm-argo-cd