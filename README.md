# Argo-cd Workflow Example

## QuickStart

`make argo-all`

## Whats here ?

Setup a k3d/k3s based local-cluster:

```sh
k3d cluster create $(CLUSTER_NAME) \ # start cluster
    --api-port 6550 \               
    --servers 1 \                    # 1 conrol-plane memeber
    --agents 2 \                     # 2 worker-plane memeber
    --port 80:80@loadbalancer \      # Forward 80 to cluster 80
    --port 443:80@loadbalancer \     # Forward 443 to cluster 80
    --wait \
    || \ # or
    k3d cluster list | grep $(CLUSTER_NAME) \
    && \ # else
    k3d cluster start $(CLUSTER_NAME)
```

## Install ArgoCD

- local argo will be available at [argocd.k3d.localhost](http://argocd.k3d.localhost/)
- admin/password
- argocd app from public git repo

```sh
kubectl create ns argocd
helm upgrade argocd --install \
		--namespace argocd \
		--version $(ARGOCD_CHART_VERTION) \
	  	argo/argo-cd \
		-f assets/charts/argo-cd/values.yaml \
		-f assets/charts/argo-cd/override.yaml \
		-f assets/charts/argo-cd/dev-mode.yaml
```

## Install Prometheus

- install prometheus server
- local promethes will be available at [prom.k3d.localhost](http://prom.k3d.localhost/) 

```sh
helm upgrade prometheus --install \
    --namespace prometheus \
    --create-namespace \
    --version $(PROMETHEUS_CHART_VERSION) \
    prometheus-community/prometheus \
    -f assets/charts/prometheus/values.yaml \
    -f assets/charts/prometheus/override.yaml
```